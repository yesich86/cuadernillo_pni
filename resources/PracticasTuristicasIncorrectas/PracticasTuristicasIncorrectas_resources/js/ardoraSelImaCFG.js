//Creado con Ardora - www.webardora.net
//bajo licencia Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
//para otros usos contacte con el autor
var timeAct=360; timeIni=360; timeBon=0;
var successes=0; successesMax=2; attempts=0; attemptsMax=1;
var score=0; scoreMax=2; scoreInc=1; scoreDec=1
var typeGame=0;
var tiTime=true;
var tiTimeType=2;
var tiButtonTime=true;
var textButtonTime="Comenzar";
var tiSuccesses=true;
var tiAttempts=false;
var tiScore=true;
var startTime;
var colorBack="#FFFDFD"; colorButton="#91962F"; colorText="#000000"; colorSele="#FF8000";
var goURLNext=false; goURLRepeat=false;tiAval=false;
var scoOk=0; scoWrong=0; scoOkDo=0; scoWrongDo=0; scoMessage=""; scoPtos=10;
var fMenssage="Tahoma, Geneva, sans-serif";
var fActi="Tahoma, Geneva, sans-serif";
var fPreg="Tahoma, Geneva, sans-serif";
var fEnun="Tahoma, Geneva, sans-serif";
var timeOnMessage=3; messageOk="¡Muy bien!"; messageTime="¡Se acabó el tiempo!";messageError="Volvé a intentar";messageErrorG="Volvé a intentar"; messageAttempts=""; isShowMessage=false;
var urlOk=""; urlTime=""; urlError=""; urlAttempts="";
var goURLOk="_blank"; goURLTime="_blank"; goURLAttempts="_blank"; goURLError="_blank"; 
borderOk="#008000"; borderTime="#FF0000";borderError="#FF0000"; borderAttempts="#FF0000";
var actMaxWidth="600"; actMaxHeight="";indexQ=0;dirMedia="PracticasTuristicasIncorrectas_resources/media/";
var quest=[["","","CuidadoMedioAmbiente.jpg"],["","","CuidarALosAnimalesII.jpg"]];
var altQuest=["",""];
var media=[["MQ==","UmVjaWNsYXI=","0_cestos_reciclado.jpg",""],["Mg==","UGxhbnRhciDDoXJib2xlcw==","0_CuidadoMedioAmbienteII.jpg",""],["Mw==","UmVzcGV0YXIgbGFzIG5vcm1hcyBkZWwgbHVnYXI=","0_cuidadoDelMedioAmbiente.jpg",""],["NA==","UmVzcGV0YXIgbGEgdmVsb2NpZGFk","0_cuidarLaFauna.jpg",""],["NQ==","QWxpbWVudGFyIGEgbG9zIGFuaW1hbGVz","0_AlimentandoACoaties.jpg",""],["Ng==","RGVmb3Jlc3Rhcg==","0_deforestacion.jpg",""]];
var alt=["","","","","",""];
var dat=[["Mg==","Mw==","MA==","MQ==","MQ==","MA=="],["Mg==","NA==","MA==","MQ==","MQ==","MA=="],["Mg==","NQ==","MQ==","MQ==","MQ==","MA=="],["MQ==","MQ==","MA==","MQ==","MQ==","MA=="],["MQ==","Mg==","MA==","MQ==","MQ==","MA=="],["MQ==","Ng==","MQ==","MQ==","MQ==","MA=="]];
var actualBoard=[];actualState=[];indexGame=1;tiAudio=false;
var wordsGame="UHJhY3RpY2FzVHVyaXN0aWNhc0luY29ycmVjdGFz"; wordsStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
function giveZindex(typeElement){var valueZindex=0; capas=document.getElementsByTagName(typeElement);
for (i=0;i<capas.length;i++){if (parseInt($(capas[i]).css("z-index"),10)>valueZindex){valueZindex=parseInt($(capas[i]).css("z-index"),10);}}return valueZindex;}
var actorder=[1,2];var in_act=0;
