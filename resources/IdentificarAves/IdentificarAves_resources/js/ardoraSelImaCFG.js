//Creado con Ardora - www.webardora.net
//bajo licencia Attribution-NonCommercial-NoDerivatives 4.0 International (CC BY-NC-ND 4.0)
//para otros usos contacte con el autor
var timeAct=360; timeIni=360; timeBon=0;
var successes=0; successesMax=3; attempts=0; attemptsMax=2;
var score=0; scoreMax=3; scoreInc=1; scoreDec=1
var typeGame=0;
var tiTime=true;
var tiTimeType=2;
var tiButtonTime=true;
var textButtonTime="Comenzar";
var tiSuccesses=true;
var tiAttempts=false;
var tiScore=true;
var startTime;
var colorBack="#FFFDFD"; colorButton="#91962F"; colorText="#000000"; colorSele="#FF8000";
var goURLNext=false; goURLRepeat=false;tiAval=false;
var scoOk=0; scoWrong=0; scoOkDo=0; scoWrongDo=0; scoMessage=""; scoPtos=10;
var fMenssage="Tahoma, Geneva, sans-serif";
var fActi="Tahoma, Geneva, sans-serif";
var fPreg="Tahoma, Geneva, sans-serif";
var fEnun="Tahoma, Geneva, sans-serif";
var timeOnMessage=3; messageOk="¡Muy bien!"; messageTime="¡Se acabó el tiempo!";messageError="Volvé a intentar";messageErrorG="Volvé a intentar"; messageAttempts=""; isShowMessage=false;
var urlOk=""; urlTime=""; urlError=""; urlAttempts="";
var goURLOk="_blank"; goURLTime="_blank"; goURLAttempts="_blank"; goURLError="_blank"; 
borderOk="#008000"; borderTime="#FF0000";borderError="#FF0000"; borderAttempts="#FF0000";
var actMaxWidth="600"; actMaxHeight="";indexQ=0;dirMedia="IdentificarAves_resources/media/";
var quest=[["","","Aves_iguazu.jpg"],["","X","siluetaAveOculta.jpg"],["","","CascadaYVencejos.jpg"]];
var altQuest=["","",""];
var media=[["MQ==","QmF0YXLDoSBkZSBwZWNobyBuZWdybw==","0_BataraPechoNegro.jpg",""],["Mg==","Q2h1cnJpbmNoZQ==","0_Churrinche.jpg",""],["Mw==","R2FyemEgQnVleWVyYQ==","0_GarcitaBueyera.jpg",""],["NA==","TWluaXZldCBFc2NhcmxhdGE=","0_ScarletMinivet.jpg",""],["NQ==","TGVjaHV6YQ==","0_Lechuza.jpg",""],["Ng==","VHVjYW4=","0_Tucan.jpg",""],["Nw==","VXJyYWNh","0_Urraca.jpg",""],["OA==","WWFwdQ==","0_Yapu.jpg",""],["OQ==","VmVuY2Vqbw==","0_VencejoDeCascada.jpg",""]];
var alt=["","","","","","","","",""];
var dat=[["MQ==","MQ==","MQ==","MQ==","MQ==","MA=="],["MQ==","Mg==","MQ==","MQ==","MQ==","MA=="],["MQ==","Mw==","MA==","MQ==","MQ==","MA=="],["Mg==","NQ==","MQ==","MQ==","MQ==","MA=="],["Mg==","Nw==","MA==","MQ==","MQ==","MA=="],["Mg==","OA==","MA==","MQ==","MQ==","MA=="],["Mw==","NA==","MA==","MQ==","MQ==","MA=="],["Mw==","Nw==","MA==","MQ==","MQ==","MA=="],["Mw==","OQ==","MQ==","MQ==","MQ==","MA=="]];
var actualBoard=[];actualState=[];indexGame=1;tiAudio=false;
var wordsGame="SWRlbnRpZmljYXJBdmVz"; wordsStr = "ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789+/=";
function giveZindex(typeElement){var valueZindex=0; capas=document.getElementsByTagName(typeElement);
for (i=0;i<capas.length;i++){if (parseInt($(capas[i]).css("z-index"),10)>valueZindex){valueZindex=parseInt($(capas[i]).css("z-index"),10);}}return valueZindex;}
var actorder=[1,2,3];var in_act=0;
